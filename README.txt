Server part:
CS.Abstraction - abstraction for Domain Logic
CS.DefaultCalculator - implementation of Domain Logic
CS.DefaultCalculator.Test - unit tests Domain Logic
CS.Utils - is empty, for example: it contains shared utils without Domain Logic
CS.WebAPI - web api (Net. Core).I use GET methods, because this methods don't change information on server.

UI part:
Folder - front contains all UI part.
Modules:
app - root module
about - lazy load module with preload stategy
calc - standart load, use resolver (OperationsResolver) emulated request to server.

CSS -boostrap

Commands for Visual Studio Code:
use : npm run build - create production version
use : npn run start - for work with UI and use proxy.config.json for config proxy
use : dotnet run - run server part

Build steps:
1. Open VS Code - root folder
2. cd front
3. npm install
4. npm run build
5. cd ..
6. cd .\CS.WebAPI\
7. dotnet run

