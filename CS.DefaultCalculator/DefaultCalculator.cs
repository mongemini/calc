﻿using CS.Abstraction.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CS.DefaultCalculator
{
    public class DefaultCalculator : ICalculator
    {
        public Task<int> AddAsync(int value1, int value2)
        {
            return Task.Run(() =>
            {
                Thread.Sleep(2000);
                return value1 + value2;
             });
        }

        public Task<int> SubstractAsync(int value1, int value2)
        {
            return Task.Run(() =>
            {
                Thread.Sleep(2000);
                return value1 - value2;
            });
        }

        public Task<int> MultiplyAsync(int value1, int value2)
        {
            return Task.Run(() =>
            {
                Thread.Sleep(2000);
                return value1 *value2;
            });
        }

        public Task<double> DivideAsync(int value1, int value2)
        {
            return Task.Run(() =>
            {
                Thread.Sleep(2000);
                if (value2 == 0)
                    throw new DivideByZeroException();
                return (double)value1 / value2;
            });
        }
    }
}
