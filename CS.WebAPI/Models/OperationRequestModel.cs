﻿using System.ComponentModel.DataAnnotations;

namespace CS.WebAPI.Models
{
    public class OperationRequestModel
    {
        [Required] //add for show server validation
        public int? Value1 { get; set; }
        [Required] //add for show server validation
        public int? Value2 { get; set; }
    }
}
