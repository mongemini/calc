﻿using CS.Abstraction.Interfaces;
using CS.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CS.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/calculator")]
    public class CalculatorController : Controller
    {
        private readonly ICalculator _calculatorService;
        public CalculatorController(ICalculator calculatorService){
            _calculatorService = calculatorService;
        }

        [HttpGet]
        [Route("add")]
        public async Task<IActionResult> Add(OperationRequestModel request)
        {
            if (ModelState.IsValid)
            {
                var result = await _calculatorService.AddAsync(request.Value1.Value, request.Value2.Value);
                return Ok(result);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpGet]
        [Route("substract")]
        public async Task<IActionResult> Substract(OperationRequestModel request)
        {
            if (ModelState.IsValid)
            {
                var result = await _calculatorService.SubstractAsync(request.Value1.Value, request.Value2.Value);
                return Ok(result);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpGet]
        [Route("multiply")]
        public async Task<IActionResult> Multiply(OperationRequestModel request)
        {
            if (ModelState.IsValid)
            {
                var result = await _calculatorService.MultiplyAsync(request.Value1.Value, request.Value2.Value);
                return Ok(result);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpGet]
        [Route("divide")]
        public async Task<IActionResult> DivideAsync(OperationRequestModel request)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _calculatorService.DivideAsync(request.Value1.Value, request.Value2.Value);
                    return Ok(result);
                }
                catch (DivideByZeroException)
                {
                    return BadRequest("Error: Divide by zero.");
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }
    }
}