import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Operation } from '../classes/operation';
import { OperationRequest } from '../classes/operation-request';
import { CalculatorService } from '../services/calculator.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-calc-panel',
  templateUrl: './calc-panel.component.html',
  styleUrls: ['./calc-panel.component.css']
})
export class CalcPanelComponent implements OnInit {

  public calculatorForm: FormGroup;
  private operation: Operation;
  public result: string;
  public isError: boolean = false;

  public operations: Operation[];

  readonly operationCtrlName : string = 'operation';

  constructor(private calculatorService: CalculatorService, private fb: FormBuilder, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data
      .subscribe((data: { operations: Operation[] }) => {
        this.operations = data.operations;
      });

    this.buildForm();
  }

  private buildForm() {
    this.calculatorForm = this.fb.group({
      value1: ["", [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      value2: ["", [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      operation: ''
    });

    this.calculatorForm.controls[this.operationCtrlName].valueChanges.subscribe(item => {
      this.operation = item;
    });

    this.calculatorForm.controls[this.operationCtrlName].setValue(this.operations[0]);
  }

  onSubmit(form) {
    if (form.valid) {
      let request = new OperationRequest();
      request.value1 = form.get('value1').value;
      request.value2 = form.get('value2').value;
      request.path = form.get(this.operationCtrlName).value.path;

      this.isError = false;
      form.disable();
      this.calculatorService.calculate(request)
        .then((result) => {
          this.result = result.json();
          form.enable();
        })
        .catch((error) => {
          this.isError = true;
          form.enable();
          this.result = error.json();
        });
    }
  }

}
