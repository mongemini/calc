import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalcPanelComponent } from './calc-panel/calc-panel.component';

const routes: Routes = [
  { path: "calc", component: CalcPanelComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalcRoutingModule { }
