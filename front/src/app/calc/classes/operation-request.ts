export class OperationRequest {
    value1: Number;
    value2: Number;
    path: string;
}
