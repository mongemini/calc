import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { CalcRoutingModule } from './calc-routing.module';
import { CalcPanelComponent } from './calc-panel/calc-panel.component';
import { CalculatorService } from './services/calculator.service';
import { OperationsResolver } from './services/operations-resolver.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CalcRoutingModule
  ],
  providers: [CalculatorService, OperationsResolver],
  declarations: [CalcPanelComponent]
})
export class CalcModule { }
