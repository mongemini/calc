import { Injectable } from '@angular/core';
import { Http, RequestOptions, URLSearchParams } from "@angular/http";
import { OperationRequest } from '../classes/operation-request';


@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  constructor(private http: Http) { }

  calculate(request: OperationRequest): Promise<any> {
    let params: URLSearchParams = new URLSearchParams();
    params.set('value1', request.value1.toString());
    params.set('value2', request.value2.toString());
    return this.http.get(request.path, { search: params }).toPromise();
  }
}
