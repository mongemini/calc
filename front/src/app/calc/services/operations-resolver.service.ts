import { Injectable } from '@angular/core';
import { Router, Resolve } from '@angular/router';

import { Operation } from '../classes/operation';
import { Observable } from 'rxjs';


@Injectable()
export class OperationsResolver implements Resolve<Operation[]> {
    constructor(private router: Router) { }

    resolve(): Observable<Operation[]> {

        return new Observable<Operation[]>(observer => {
            setTimeout(function () {
                observer.next([
                    { name: "Add", path: "api/calculator/add" },
                    { name: "Subtract", path: "api/calculator/substract" },
                    { name: "Multiply", path: "api/calculator/multiply" },
                    { name: "Divide", path: "api/calculator/divide" }
                ]);
                observer.complete();

            }, 100);
        });
    }
}