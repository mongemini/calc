import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutPanelComponent } from './about-panel/about-panel.component';

const routes: Routes = [
  { path: "", component: AboutPanelComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutRoutingModule { }
