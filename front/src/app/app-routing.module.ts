import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { CalcPanelComponent } from './calc/calc-panel/calc-panel.component';
import { OperationsResolver } from './calc/services/operations-resolver.service';


const routes: Routes = [
  {
    path: "",
    redirectTo: "calc",
    pathMatch: "full"
  },
  {
    path: "calc",
    component: CalcPanelComponent,
    resolve: {
      operations: OperationsResolver
    }
  },
  {
    path: "about",
    loadChildren: "./about/about.module#AboutModule"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules
  })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
