﻿using CS.Abstraction.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace CS.DefaultCalculator.Test
{
    [TestClass]
    public class DefaultCalculatorTest
    {
        private readonly ICalculator _calculatorService;

        public DefaultCalculatorTest()
        {
            _calculatorService = new DefaultCalculator();
        }

        [TestMethod]
        public async Task AddTest()
        {
            var result = await _calculatorService.AddAsync(5,5);

            Assert.AreEqual(10, result);
        }

        [TestMethod]
        public async Task SubstractTest()
        {
            var result = await _calculatorService.SubstractAsync(10, 5);

            Assert.AreEqual(5, result);
        }

        [TestMethod]
        public async Task MultiplyTest()
        {
            var result = await _calculatorService.MultiplyAsync(5, 5);

            Assert.AreEqual(25, result);
        }

        [TestMethod]
        public async Task DivideTest()
        {
            var result = await _calculatorService.DivideAsync(10, 5);

            Assert.AreEqual(2, result);
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        public async Task DivideThrowTest()
        {
            var result = await _calculatorService.DivideAsync(10, 0);
        }
    }
}
