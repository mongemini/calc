﻿using System.Threading.Tasks;

namespace CS.Abstraction.Interfaces
{
    public interface ICalculator
    {
        Task<int> AddAsync(int value1, int value2);

        Task<int> SubstractAsync(int value1, int value2);

        Task<int> MultiplyAsync(int value1, int value2);

        Task<double> DivideAsync(int value1, int value2);
    }
}
